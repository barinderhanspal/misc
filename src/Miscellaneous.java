import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Stack;


public class Miscellaneous {

	public static float sqrt(int n){

		float low = 0, high = n;
		float mid = low + (float)((high - low) / 2.0);

		while(Math.abs(mid*mid - n) > 0.000001){
			if(mid*mid < n){
				low = mid;
			} else if (mid*mid > n){
				high = mid;
			}

			mid = low + (float)((high - low) / 2.0);
		}

		return mid;
	}

	public static int powerNumber(int number, int power){
		int increment = number;
		int result = number;

		for(int powerIndex = 1; powerIndex < power; powerIndex++){
			for(int incrementIndex = 1; incrementIndex < number; incrementIndex ++){
				result += increment;
			}
			increment = result;
		}

		return result;
	}

	public static int fibRec(int n){
		if( n <= 1)
			return n;

		return fibRec(n - 1) + fibRec(n - 2);
	}


	public static int fibIter(int n){

		if(n == 1 || n == 0)
			return n;

		int prev = 1;
		int prevprev = 0;
		int result = 0;
		for(int i = 2; i <= n; i++){
			result = prev + prevprev;
			prevprev = prev;
			prev = result;
		}

		return result;
	}

	public static int fibMatrix(int n){
		if(n == 0 || n == 1){
			return n;
		}
		int result[][] = {{1,1},{1,0}};
		power(result, n);
		return result[0][1];
	}

	public static void power(int[][] matrix, int n){

		if (n == 0 || n == 1)
			return;

		int base[][] = {{1,1},{1,0}};

		power(matrix, n/2);
		multiply(matrix, matrix);

		if(n % 2 != 0)
			multiply(matrix, base);

	}

	/**
	 * b is a subset of a?
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static boolean isSubset(int[] a, int b[]){

		int i = 0, j = 0;

		while(i < a.length && j < b.length){
			if(a[i] == b[j]){
				i++; j++;
			} else if (a[i] < b[j]){
				i++;
			} else {
				break;
			}
		}
		/*System.out.println(j);
		System.out.println(b.length);
		 */return (j == (b.length));
	}

	/*	public static boolean isSubsetColl(int[] a, int[] b){
		LinkedList<Integer> setA = Ints.asList(a);

	}*/

	public static void printCombinations(String input){
		printCombinationsHelper("", input);
	}

	private static void printCombinationsHelper(String prefix, String substring) {
		if(substring.length() == 0)
			System.out.println(prefix);
		else {
			for(int i=0; i<substring.length(); i++){
				printCombinationsHelper(prefix + substring.charAt(i),
						substring.substring(0, i) + 
						substring.substring(i+1,substring.length()));
			}
		}
	}

	public static void print2Com(String input){

		print2ComHelper("",input);
	}


	private static void print2ComHelper(String prefix, String input) {
		if(prefix.length() == 3)
			System.out.println(prefix);
		else {
			for(int i=0; i<input.length(); i++){
				print2ComHelper(prefix + input.charAt(i), input);
			}
		}

	}

	public static void multiply(int a[][], int b[][]){
		int x = a[0][0] * b[0][0] + a[0][1] * b[1][0];
		int y = a[0][0] * b[0][1] + a[0][1] * b[1][1];
		int z = a[1][0] * b[0][0] + a[1][0] * b[1][0];
		int w = a[1][0] * b[0][1] + a[1][1] * b[1][1];

		a[0][0] = x;
		a[0][1] = y;
		a[1][0] = z;
		a[1][1] = w;
	}

	public static void printSquares(int n)
	{
		// Initialize 'square' and previous value of 'x'
		int square = 0, prev_x = 0;

		// Calculate and print squares
		for (int x = 0; x < n; x++)
		{
			// Update value of square using previous value
			square = (square + x + prev_x);

			// Print square and update prev for next iteration
			System.out.println(square);;
			prev_x = x;
		}
	}

	public static int findDuplicate(int a[]){

		if(a.length <= 0)
			return -1;

		int dupVal = a[0];
		int count = 1;

		for(int i = 1; i < a.length; i++){
			if(a[i] == dupVal){
				count ++;
				if(count == 5)
					break;
			} else {
				count --;
			}

			if(count <= 0){
				count = 1;
				dupVal = a[i];
			}
		}

		return dupVal;
	}

	/* Write a function to remove the duplicated characters from a string, and maintain the order of the characters. 

ex. �abracadabra� ? �abrcd�
	 */
	public static String removeDuplicatesFromString(String input){
		LinkedHashSet<Character> set = new LinkedHashSet<Character>();

		for(int i = 0; i < input.length(); i++){
			set.add(input.charAt(i));
		}

		StringBuilder sb = new StringBuilder("");

		for(Character c : set){
			sb.append(c);
		}

		return sb.toString();
	}

	public static int evaluateInfixExpression(String input){
		int result = 0;
		Stack<Integer> operandStack = new Stack<Integer>();
		Stack<Character> operatorStack = new Stack<Character>();

		for(int index = 0; index < input.length(); index++){

			char c = input.charAt(index);


			if(Character.isDigit(c)){
				int endIndex = index;
				Integer number = Integer.parseInt(""+c);
				while( endIndex < input.length() -1 ){
					if( Character.isDigit(input.charAt(++endIndex))){
						number = Integer.parseInt(input.substring(index, endIndex));
					} else {
						break;
					}
				}
				index = endIndex;
				System.out.println(number + " is number");
				operandStack.push(number);
			} else if (c == '*' || c == '/' || c == '+' || c == '-'){

				while(!operandStack.empty() && !operatorStack.empty() 
						&& (presOf(operatorStack.peek()) >= presOf(c))){
					int op2 = operandStack.pop();
					int op1 = operandStack.pop();
					result = calculate(op1, op2, operatorStack.pop());
					operandStack.push(result);
				}

				operatorStack.push(c);
			}
		}

		while(!operatorStack.empty() && !operandStack.empty()){
			int op2 = operandStack.pop();
			int op1 = operandStack.pop();
			result = calculate(op1, op2, operatorStack.pop());
			operandStack.push(result);
		}

		return operandStack.pop();
	}

	private static int calculate(int op1, int op2, Character operator) {
		System.out.println("Calculating " + op1 + operator+op2);
		switch (operator){
		case '*' : return op1 * op2;
		case '/' : return op1 / op2;
		case '+' : return op1 + op2;
		case '-' : return op1 - op2;

		default: return -10000;
		}
	}

	private static int presOf(Character operator) {
		switch(operator){
		case '*' :
		case '/' : return 2;

		case '+' :
		case '-' : return 1;

		default : return 0;
		}
	}


	public static void printAllPaths(int[][] matrix){
		printPath(matrix, 0, 0, "");
	}

	public static void printPath(int[][] matrix, int row, int column, String output){

			if(output.length() == 0)
				output += matrix[row][column];
			else
				output += "-" + matrix[row][column];
			
			if(row == matrix.length - 1){
				System.out.println(output);
				return;
			}
		
			if(column > 0)
				printPath(matrix, row + 1, column - 1, output);
			
			printPath(matrix, row + 1, column, output);
			
			if(column < matrix[0].length - 1)
				printPath(matrix, row + 1, column + 1, output);
	}

	public static void main(String args[]){
		
		int[][]A = {{1,2,3},{4,5,6},{7,8,9}, {10, 11, 12}};
		
		//printAllPaths(A);
		//System.out.println(sqrt(9));
		//System.out.println(fibRec(5));
		//System.out.println(fibMatrix(5));
		//System.out.println(fibIter(10));

		/*int[] a = new int[]{1,5,8,10,20,21};
		int[] b = new int[]{5,8,10};

		System.out.println(isSubset(a, b));*/
		//printSquares(5);
		print2Com("abc");
		//System.out.println(powerNumber(5, 3));

		/*int a[] = new int[]{1,2,1,3,1,4,1,5,1,6};
		System.out.println(findDuplicate(a));*/

	//	System.out.println(evaluateInfixExpression("2*3"));
		//System.out.println(removeDuplicatesFromString("abracadabra"));
	}


}
